import React from "react";
import PropTypes from "prop-types";
import { MDXRenderer } from "gatsby-plugin-mdx";

import {
  useIdentityContext,
  IdentityContextProvider,
} from "react-netlify-identity-widget";

import Layout from "../Layout";
import SEO from "../SEO";
import PostNav from "./PostNav";
import EditGithub from "./EditGithub";

function AuthStatusView({ body, roleAccess }) {
  const identity = useIdentityContext();
  return (
    <div>
      <div>
        {identity && identity.user ? (
          identity.user.app_metadata.roles.some((e) => e === roleAccess) ? (
            <MDXRenderer>{body}</MDXRenderer>
          ) : (
            <p>Ainda não tens acesso a esta secção.</p>
          )
        ) : (
          <p>Não tens acesso a esta secção. Inicia sessão primeiro</p>
        )}
      </div>
    </div>
  );
}

export default function Docs({ mdx, pageContext }) {
  const url = "https://(...).treetree2.school";
  const { prev, next, githubEditUrl } = pageContext;
  const { title, description, image, disableTableOfContents } = mdx.frontmatter;
  const { headings, body } = mdx;
  const { slug } = mdx.fields;

  return (
    <>
      <SEO title={title} description={description} slug={slug} image={image} />
      <Layout
        disableTableOfContents={disableTableOfContents}
        title={title}
        headings={headings}
      >
        <IdentityContextProvider url={url}>
          <AuthStatusView body={body} roleAccess={image} />
        </IdentityContextProvider>
        <EditGithub githubEditUrl={githubEditUrl} />
        <PostNav prev={prev} next={next} />
      </Layout>
    </>
  );
}

Docs.propTypes = {
  mdx: PropTypes.shape({
    body: PropTypes.string,
    headings: PropTypes.array,
    frontmatter: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
      image: PropTypes.string,
      disableTableOfContents: PropTypes.bool,
    }),
    fields: PropTypes.shape({
      slug: PropTypes.string,
    }),
  }).isRequired,
  pageContext: PropTypes.shape({
    prev: PropTypes.shape({}),
    next: PropTypes.shape({}),
    githubEditUrl: PropTypes.string,
  }).isRequired,
};
