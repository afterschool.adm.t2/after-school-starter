import styled from "@emotion/styled";
import logo from "../assets/minibanner.png";
export default styled.div`
  width: 100%;
  height: 100%;
  background: url(${logo}) no-repeat;
  background-size: contain;
`;
