export default {
  colors: {
    primary: "#000",
    background: "#FFF",
    shape: `#F2F2FA`,
    title: `#3D3D4D`,
    text: `#6C6C80`,
  },
};
