module.exports = {
  siteMetadata: {
    siteTitle: `After School - (...)`,
    defaultTitle: `After School - (...)`,
    siteTitleShort: `after-school-(...)`,
    siteDescription: `Site do After School (...)`,
    siteUrl: `https://(...).treetree2.school`,
    siteAuthor: `treetree2`,
    siteImage: `/banner.png`,
    siteLanguage: `pt`,
    themeColor: `#000000`,
    basePath: `/`,
  },
  plugins: [
    {
      resolve: `@rocketseat/gatsby-theme-docs`,
      options: {
        configPath: `src/config`,
        docsPath: `src/docs`,
        githubUrl: `https://gitlab.com/afterschool.adm.t2/(...)`,
        // baseDir: ``,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `After School (...)`,
        short_name: `ASO (...)`,
        start_url: `/`,
        background_color: `#ffffff`,
        display: `standalone`,
        icon: `static/favicon.png`,
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // trackingId: ``,
      },
    },
    `gatsby-plugin-remove-trailing-slashes`,
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://https://(...).treetree2.school`,
      },
    },
    `gatsby-plugin-offline`,
  ],
};
