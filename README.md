<!-- PROJECT LOGO -->
<p align="center">
  <a href="https://gitlab.com/afterschool.adm.t2/after-school-starter">
      <img src="https://static.wixstatic.com/media/e37a51_857e9186f94a496eb4aeb0c3feffd93f~mv2.png/v1/fill/w_460,h_282,al_c,q_85,usm_0.66_1.00_0.01/e37a51_857e9186f94a496eb4aeb0c3feffd93f~mv2.webp" alt="Logo" width="120" height="80">
  </a>
</p>

<h2 align="center">
    After School Gatsby Starter
</h2>

### Pré-requisitos

- git
- Node.js
- gatsby-cli
- yarn

Ver [secção do tutorial do gatsby](https://www.gatsbyjs.com/tutorial/part-zero/) para ver como instalar as ferramentas.

Apesar de o starter também funcionar com npm, é preferível usar o [yarn](https://classic.yarnpkg.com/en/docs/install/) por uma questão de consistência e rapidez.

### Criar um site para um curso

1. Criar o site

```sh
gatsby new nome-do-curso https://gitlab.com/afterschool.adm.t2/after-school-starter
```

2. Associar o site ao GitLab

- Ir ao GitLab.
- Criar um novo projeto.
- Optar por `create a blank project`.
- Criar projeto, tendo o cuidado de colocar o nome com a seguinte nomenclatura: [curso] [ano].[versão] (e.g., Machine Learning 2021.01).
- Dar autorização aos utilizadores que vão editar o site (em Members, fazer Invite Member, dando o role de Maintainer).
- Seguir as instruções da opção `push an existing folder`.

3. Iniciar o servidor de desenvolvimento.

```sh
cd nome-do-curso
gatsby develop
```

4. Aceder ao site em `http://localhost:8000` e começar a editar.

A página inicial está em `src/@rocketseat/gatsby-theme-docs/text/index.mdx` e as restantes estão em `src/docs`. A sidebar está em `src/config/sidebar.yml` (confirmar links externos, para o fórum e para o servidor no Discord).

Os ficheiros são escritos em markdown e o acesso às páginas é controlada pela tag `image` na front-matter de cada página.

Além do conteúdo, editar também o ficheiro `gatsby-config.js`:

- Substituir os `(...)` pelo nome do curso.
- Se necessário, pôr trackingId do Google Analytics.

Finalmente, se o curso tiver um banner, colocá-lo na pasta `static` com o nome `banner.png`.

5. Fazer deploy do site no netlify e ativar a Identity.

---

Baseado no [gatsby-starter-rocket-docs](https://github.com/Rocketseat/gatsby-starter-rocket-docs) da [Rocketseat](https://rocketseat.com.br/).
